package com.custom.settings;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class Settings implements Parcelable {
    public Bitmap[] loadImages;

    public Settings(Bitmap[] loadImages) {
        this.loadImages = loadImages;
    }

    public Settings(Parcel in) {
        String[] data = new String[4];
        in.readStringArray(data);

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

    public static final Parcelable.Creator<Settings> CREATOR = new Parcelable.Creator<Settings>() {

        @Override
        public Settings createFromParcel(Parcel source) {
            return new Settings(source);
        }

        @Override
        public Settings[] newArray(int size) {
            return new Settings[size];
        }
    };
}
