package com.custom.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import androidx.appcompat.view.ContextThemeWrapper;

import com.custom.settings.Settings;
import com.ag.puzzle.MainActivity;
import com.ag.puzzle.R;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class CustomTable extends TableLayout implements View.OnClickListener, Parcelable {

    public MainActivity mainActivity;
    public int difficulty;
    public int emptyButtonName;
    public int[] buttonsArray;
    public int[] randomButtonsArray;
    public Bitmap[] loadImages;
    public Map<Integer, Integer> randomButtonsCoordinate = new HashMap<>();

    public CustomTable(Context context) {
        super(context);
    }

    public CustomTable(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void showTable(MainActivity mainActivity) {
        this.difficulty = mainActivity.difficulty;
        this.emptyButtonName = difficulty * difficulty;
        if (buttonsArray == null) {
            buttonsArray = new int[emptyButtonName];
        }
        if (randomButtonsArray == null) {
            randomButtonsArray = new int[emptyButtonName];
            setRightValuesArray();
            setRandomValuesArray();
        }
        this.mainActivity = mainActivity;
        setCoordinatesForRandomValues();
        generateTable();
    }

    public void resetValues() {
        buttonsArray = null;
        randomButtonsArray = null;
    }

    @Override
    public void onClick(View v) {
        int invisibleButtonId = (int) R.string.square_button_id;
        if (loadImages == null) {
            SquareTextView buttonClick = (SquareTextView) v;
            SquareTextView buttonInvisible = mainActivity.findViewById(invisibleButtonId);
            replaceTextButton(buttonClick, buttonInvisible);
        } else {
            SquareImageView buttonClick = (SquareImageView) v;
            SquareImageView buttonInvisible = mainActivity.findViewById(invisibleButtonId);
            replaceImageButton(buttonClick, buttonInvisible);
        }
        completeGame();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

    public static final Parcelable.Creator<Settings> CREATOR = new Parcelable.Creator<Settings>() {

        @Override
        public Settings createFromParcel(Parcel source) {
            return new Settings(source);
        }

        @Override
        public Settings[] newArray(int size) {
            return new Settings[size];
        }
    };

    private void setRightValuesArray() {
        for (int i = 0; i < buttonsArray.length; i++) {
            buttonsArray[i] = i + 1;
            randomButtonsArray[i] = i + 1;
        }
    }

    private void setRandomValuesArray() {
        Random rnd = new Random();
        for (int i = randomButtonsArray.length - 2; i > 0; i--) {
            int randomIndex = rnd.nextInt(i + 1);
            int randomButtonId = randomButtonsArray[randomIndex];

            randomButtonsArray[randomIndex] = randomButtonsArray[i];
            randomButtonsArray[i] = randomButtonId;
        }
    }

    private void setCoordinatesForRandomValues() {
        int x = 1;
        int y = difficulty;
        for (int i = 0; i < randomButtonsArray.length; i++) {
            randomButtonsCoordinate.put(randomButtonsArray[i], ((x * 10) + y));
            if ((i + 1) % difficulty == 0) {
                x = 1;
                y--;
            } else {
                x++;
            }
        }
    }

    private void generateTable() {
        TableLayout table = mainActivity.findViewById(R.id.table_buttons);
        table.setGravity(Gravity.CENTER);
        int buttonIterator = 0;
        table.removeAllViews();
        for (int numberRow = 1; numberRow <= difficulty; numberRow++) {
            TableRow row = new TableRow(getContext());
            row.setGravity(Gravity.CENTER);
            for (int column = 1; column <= difficulty; column++) {
                if (loadImages == null) {
                    setTextButton(row, buttonIterator);
                } else {
                    setImageButton(row, buttonIterator);
                }
                buttonIterator++;
            }
            table.addView(row);
        }
    }

    private void setImageButton(TableRow row, int iterator) {
        ContextThemeWrapper themeContext = new ContextThemeWrapper(getContext(), R.style.imageButtonPuzzle);
        SquareImageView button = new SquareImageView(themeContext);
        int buttonId = (int) R.string.square_button_id;
        if (randomButtonsArray[iterator] == emptyButtonName) {
            button.setVisibility(View.INVISIBLE);
        } else {
            buttonId = R.string.square_button_id + randomButtonsArray[iterator];
            button.setTag(randomButtonsArray[iterator]);
            button.setImageBitmap(loadImages[randomButtonsArray[iterator] - 1]);
        }
        button.setId(buttonId);
        TableRow.LayoutParams width = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT);
        button.setLayoutParams(width);
        button.setOnClickListener(this);
        row.addView(button);
    }

    private void setTextButton(TableRow row, int iterator) {
        ContextThemeWrapper themeContext = new ContextThemeWrapper(getContext(), R.style.textButtonPuzzle);
        SquareTextView button = new SquareTextView(themeContext);
        int buttonId = (int) R.string.square_button_id;
        if (randomButtonsArray[iterator] == emptyButtonName) {
            button.setVisibility(View.INVISIBLE);
        } else {
            buttonId = R.string.square_button_id + randomButtonsArray[iterator];
            button.setTag(randomButtonsArray[iterator]);
            button.setText("" + randomButtonsArray[iterator]);
        }
        button.setId(buttonId);
        TableRow.LayoutParams params = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT);
        params.setMargins(4, 4, 4, 4);
        button.setLayoutParams(params);
        button.setOnClickListener(this);
        row.addView(button);
    }

    private void replaceImageButton(SquareImageView buttonClick, SquareImageView buttonInvisible) {
        int buttonClickTag = (int) buttonClick.getTag();
        if (canReplaceButton(emptyButtonName, buttonClickTag)) {
            int buttonInvisibilityId = buttonInvisible.getId();

            buttonInvisible.setVisibility(View.VISIBLE);
            buttonInvisible.setImageBitmap(loadImages[buttonClickTag - 1]);
            buttonInvisible.setTag(buttonClick.getTag());
            buttonInvisible.setId(buttonClick.getId());

            buttonClick.setVisibility(View.INVISIBLE);
            buttonClick.setId(buttonInvisibilityId);
            replaceCoordinates(emptyButtonName, buttonClickTag);
            replaceValuesInRandomArray(emptyButtonName, buttonClickTag);
        }
    }

    private void replaceTextButton(SquareTextView buttonClick, SquareTextView buttonInvisible) {
        int buttonClickTag = (int) buttonClick.getTag();
        if (canReplaceButton(emptyButtonName, buttonClickTag)) {
            int buttonInvisibilityId = buttonInvisible.getId();

            buttonInvisible.setVisibility(View.VISIBLE);
            buttonInvisible.setText("" + buttonClick.getTag());
            buttonInvisible.setTag(buttonClick.getTag());
            buttonInvisible.setId(buttonClick.getId());

            buttonClick.setVisibility(View.INVISIBLE);
            buttonClick.setId(buttonInvisibilityId);
            replaceCoordinates(emptyButtonName, buttonClickTag);
            replaceValuesInRandomArray(emptyButtonName, buttonClickTag);
        }
    }

    private void replaceCoordinates(int buttonInvisibleTag, int buttonClickTag) {
        int coordinateClickButton = randomButtonsCoordinate.get(buttonClickTag);
        int coordinateInvisibleButton = randomButtonsCoordinate.get(buttonInvisibleTag);
        randomButtonsCoordinate.put(buttonInvisibleTag, coordinateClickButton);
        randomButtonsCoordinate.put(buttonClickTag, coordinateInvisibleButton);
    }

    private void replaceValuesInRandomArray(int buttonInvisibleTag, int buttonClickTag) {
        int buttonInvisibleIndex = -1;
        int buttonClickIndex = -1;
        for (int i = 0; i < randomButtonsArray.length; i++) {
            if (randomButtonsArray[i] == buttonInvisibleTag) {
                buttonInvisibleIndex = i;
            }
            if (randomButtonsArray[i] == buttonClickTag) {
                buttonClickIndex = i;
            }
        }
        randomButtonsArray[buttonInvisibleIndex] = buttonClickTag;
        randomButtonsArray[buttonClickIndex] = buttonInvisibleTag;
    }

    private boolean canReplaceButton(int buttonInvisibleTag, int buttonClickTag) {
        boolean canReplaceButton = false;
        if (randomButtonsCoordinate.size() != 0) {
            int coordinateInvisibleButton = randomButtonsCoordinate.get(buttonInvisibleTag);
            int coordinateClickButton = randomButtonsCoordinate.get(buttonClickTag);
            int[] availableCoordinates = getAvailableCoordinatesButton(coordinateInvisibleButton);
            for (int i = 0; i < availableCoordinates.length; i++) {
                if (availableCoordinates[i] == coordinateClickButton) {
                    canReplaceButton = true;
                }
            }
        }

        return canReplaceButton;
    }

    private int[] getAvailableCoordinatesButton(int coordinateInvisibleButton) {
        int[] availableCoordinates = {100, 100, 100, 100};
        int x = coordinateInvisibleButton / 10;
        int y = coordinateInvisibleButton - (x * 10);
        if (x - 1 > 0) {
            availableCoordinates[0] = (x - 1) * 10 + y;
        }
        if (x + 1 < difficulty + 1) {
            availableCoordinates[1] = (x + 1) * 10 + y;
        }
        if (y - 1 > 0) {
            availableCoordinates[2] = (x * 10) + (y - 1);
        }
        if (y + 1 < difficulty + 1) {
            availableCoordinates[3] = (x * 10) + y + 1;
        }

        return availableCoordinates;
    }

    private void completeGame() {
        if (Arrays.equals(buttonsArray, randomButtonsArray)) {
            randomButtonsCoordinate.clear();
            Toast.makeText(getContext(), R.string.congratulations, Toast.LENGTH_LONG).show();
        }
    }
}
