package com.custom.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.preference.PreferenceManager;

import com.ag.puzzle.MainActivity;
import com.ag.puzzle.R;

public class DifficultyDialog extends DialogFragment {

    public static final int DEFAULT_DIFFICULTY = 3;
    public static final String DIFFICULTY_STATE = "difficulty";
    final String DEFAULT_DIFFICULTY_STRING = "3";

    SharedPreferences settings;

    private String selectValue;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if (getActivity() != null) {
            settings = PreferenceManager.getDefaultSharedPreferences(getActivity());
            selectValue = settings.getString(DIFFICULTY_STATE, DEFAULT_DIFFICULTY_STRING);
        }
        builder.setTitle(R.string.set_difficulty)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (getActivity() != null) {
                            SharedPreferences.Editor prefEditor = settings.edit();
                            prefEditor.putString(DIFFICULTY_STATE, selectValue);
                            prefEditor.apply();
                            Intent mainActivity = new Intent(getActivity(), MainActivity.class);
                            startActivity(mainActivity);
                        }
                    }
                })
                .setSingleChoiceItems(R.array.settings_entries, getSelectedItem(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        String[] settings = getResources().getStringArray(R.array.settings_values);
                        selectValue = settings[item];
                    }
                });

        return builder.create();
    }

    private int getSelectedItem() {
        int selectedItem = 0;
        switch (selectValue) {
            case "4":
                selectedItem = 1;
                break;
            case "5":
                selectedItem = 2;
                break;
        }

        return selectedItem;
    }
}
