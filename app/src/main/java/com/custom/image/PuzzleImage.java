package com.custom.image;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.ag.puzzle.MainActivity;
import com.ag.puzzle.R;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class PuzzleImage {
    public static final int REQUEST_PHOTO_CAMERA = 1;
    public static final int REQUEST_GALLERY_PHOTO = 2;
    public static final int PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 135;

    public Uri sourceUri;
    public Uri destinationUri;
    public File newFileForCamera;
    public MainActivity mainActivity;
    public Bitmap[] loadImages;
    public int difficulty;

    public PuzzleImage(int difficulty, MainActivity mainActivity) {
        this.difficulty = difficulty;
        this.mainActivity = mainActivity;
        setDifficulty(difficulty);
        setNewFileAndUri();
    }

    public void openCropActivity() {
        UCrop.Options options = new UCrop.Options();
        options.setCircleDimmedLayer(true);
        options.setCropFrameColor(ContextCompat.getColor(mainActivity, R.color.colorAccent));
        UCrop.of(sourceUri, destinationUri)
                .withAspectRatio(5f, 5f)
                .start(mainActivity);
    }

    public void openGallery() {
        Intent pictureIntent = new Intent(Intent.ACTION_GET_CONTENT);
        pictureIntent.setType("image/*");
        pictureIntent.addCategory(Intent.CATEGORY_OPENABLE);
        String[] mimeTypes = new String[]{"image/jpeg", "image/png"};
        pictureIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        mainActivity.startActivityForResult(Intent.createChooser(pictureIntent, "Select Image"),
                PuzzleImage.REQUEST_GALLERY_PHOTO);
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
        loadImages = new Bitmap[this.difficulty * this.difficulty];
    }

    public Bitmap[] getCropImagesArray(Uri imageUri) {
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(mainActivity.getContentResolver(), imageUri);
            int originalImageSize = bitmap.getWidth();
            Bitmap originalImg = Bitmap.createBitmap(
                    bitmap,
                    0,
                    0,
                    originalImageSize,
                    originalImageSize,
                    getExifAngle(imageUri),
                    true);
            int cropSize = originalImageSize / difficulty;
            int iterator = 0;
            for (int numberRow = 1; numberRow <= difficulty; numberRow++) {
                int offsetY = cropSize * (numberRow - 1);
                for (int column = 1; column <= difficulty; column++) {
                    int offsetX = cropSize * (column - 1);
                    Bitmap cropImg = Bitmap.createBitmap(
                            originalImg,
                            offsetX,
                            offsetY,
                            cropSize,
                            cropSize);
                    loadImages[iterator] = cropImg;

                    iterator++;
                }
            }
        } catch (IOException e) {
            Toast.makeText(mainActivity, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        return loadImages;
    }

    private Matrix getExifAngle(Uri uri) {
        int angle = 0;
        try {
            InputStream stream = mainActivity.getContentResolver().openInputStream(uri);
            ExifInterface ei = new ExifInterface(stream);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    angle = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    angle = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    angle = 270;
                    break;
            }
        } catch (Exception e) {
            Toast.makeText(mainActivity, e.getMessage(), Toast.LENGTH_LONG).show();
        }
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);

        return matrix;
    }

    private void setNewFileAndUri() {
        String imageFileName = "puzzle_crop_" + System.currentTimeMillis();
        String absolutePath = "";
        try {
            File storageDir = mainActivity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File file = File.createTempFile(imageFileName, ".jpg", storageDir);
            absolutePath = "file:" + file.getAbsolutePath();
            newFileForCamera = file;
        } catch (IOException ex) {
            Toast.makeText(mainActivity, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }

        sourceUri = Uri.parse(absolutePath);
        destinationUri = sourceUri;
    }
}
