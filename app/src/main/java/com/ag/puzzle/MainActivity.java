package com.ag.puzzle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.preference.PreferenceManager;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.custom.dialog.DifficultyDialog;
import com.custom.dialog.ImageDialog;
import com.custom.image.PuzzleImage;
import com.custom.view.CustomTable;
import com.yalantis.ucrop.UCrop;

public class MainActivity extends AppCompatActivity {

    public static final String CUSTOM_TABLE = "custom_table";

    public int difficulty = DifficultyDialog.DEFAULT_DIFFICULTY;
    public CustomTable customTable;
    public PuzzleImage puzzleImage;
    public ImageDialog selectImageDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        customTable = new CustomTable(this);
        customTable.showTable(this);
        puzzleImage = new PuzzleImage(difficulty, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PuzzleImage.REQUEST_PHOTO_CAMERA && resultCode == RESULT_OK) {
            puzzleImage.sourceUri = puzzleImage.destinationUri;
            puzzleImage.openCropActivity();
        }
        if (requestCode == PuzzleImage.REQUEST_GALLERY_PHOTO && resultCode == RESULT_OK && data != null) {
            puzzleImage.sourceUri = data.getData();
            puzzleImage.openCropActivity();
        }
        if (requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_OK && data != null) {
            customTable.loadImages = puzzleImage.getCropImagesArray(UCrop.getOutput(data));
            selectImageDialog.dismiss();
            resetGame();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PuzzleImage.PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    puzzleImage.openGallery();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_reset_game:
                resetGame();
                return true;
            case R.id.load_image:
                showDialogSelectImage();
                return true;
            case R.id.open_settings:
                showDialogDifficulty();
                return true;
//            case R.id.square_button_id:
//                Intent cameraActivity = new Intent(this, CameraActivity.class);
//                startActivity(cameraActivity);
//                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences defaultSettings = PreferenceManager.getDefaultSharedPreferences(this);
        int newDifficulty = Integer.parseInt(defaultSettings.getString("difficulty", "3"));
        if (difficulty != newDifficulty) {
            difficulty = newDifficulty;
            Toast.makeText(this, R.string.message_before_set_new_difficulty, Toast.LENGTH_LONG).show();
            resetGame();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(CUSTOM_TABLE, customTable);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        savedInstanceState.isEmpty();
        customTable = savedInstanceState.getParcelable(CUSTOM_TABLE);
        if (customTable != null) {
            customTable.showTable(this);
        }
    }

    public void showDialogDifficulty() {
        DifficultyDialog dialog = new DifficultyDialog();
        dialog.show(getSupportFragmentManager(), "difficulty");
    }

    public void showDialogSelectImage() {
        selectImageDialog = new ImageDialog();
        selectImageDialog.show(getSupportFragmentManager(), "select-image");
    }

    public void resetGame() {
        puzzleImage.setDifficulty(difficulty);
        customTable.resetValues();
        customTable.showTable(this);
    }

    public void openCamera(View view) {
        Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri uri = FileProvider.getUriForFile(
                this,
                "com.ag.android.fileprovider",
                puzzleImage.newFileForCamera);
        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(pictureIntent, PuzzleImage.REQUEST_PHOTO_CAMERA);
    }

    public void openGallery(View view) {
        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            }
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    PuzzleImage.PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        } else {
            puzzleImage.openGallery();
        }
    }
}